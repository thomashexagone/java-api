package com.exam.commande.commande;

import javax.persistence.*;

@Entity
public class LigneCommande {
    @Id
    @GeneratedValue
    public long id;

    public int quantite;

    public long produit_id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public long getProduit_id() {
        return produit_id;
    }

    public void setProduit_id(long produit_id) {
        this.produit_id = produit_id;
    }
}
