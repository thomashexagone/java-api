package com.exam.commande.commande;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LigneCommandeRepository extends CrudRepository<LigneCommande, Long> {
}
