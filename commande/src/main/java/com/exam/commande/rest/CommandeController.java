package com.exam.commande.rest;

import com.exam.commande.commande.Commande;
import com.exam.commande.commande.CommandeRepository;
import com.exam.commande.commande.LigneCommande;
import com.exam.commande.commande.LigneCommandeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class CommandeController {

    @Autowired
    CommandeRepository commandeRepository;

    @Autowired
    LigneCommandeRepository ligneCommandeRepository;

    /**
     * La fonction permet de créé une commande et de mettre par défaut le statut de la commande sur "A LIVRER".
     * @param commande est un objet commande.
     */
    @PostMapping("/commande")
    public void postCommande(@RequestBody Commande commande){
        commande.setStatut("A LIVRER");
        commandeRepository.save(commande);
    }

    /**
     * La fonction permet d'afficher la liste de toutes les commandes.
     * @return la liste des commandes.
     */
    @GetMapping(value = "/commandes", produces = APPLICATION_JSON_VALUE)
    public Iterable<Commande> getCommande(){
        return commandeRepository.findAll();
    }

    /**
     * La fonction permet d'afficher une commande en fonction de son id.
     * @param id est l'id de la commande que nous voulons afficher.
     * @return une commande en fonction de son id.
     */
    @GetMapping("/commandes/{id}")
    public Commande getCommandeById(@PathVariable Long id){
        return commandeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    /**
     * La fonction permet de supprimer une commande en fonction de son id.
     * @param id est l'id de la commande à supprimer.
     */
    @DeleteMapping(path = "/commandes/{id}")
    public void deleteCommande(@PathVariable Long id){
        commandeRepository.deleteById(id);
    }

    /**
     * La fonction permet de mettre à jour une commande en fonction de son id.
     * @param id est l'id de la commande a mettre à jour.
     * @param commande est un objet commande.
     * @return la commande avec les nouvelles valeurs.
     */
    @PutMapping(path = "/commandes/update/{id}")
    public Commande updateCommande(@PathVariable long id, @RequestBody Commande commande){
        Commande updatecommande = commandeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        updatecommande = commande;
        commandeRepository.save(updatecommande);
        return updatecommande;
    }

    /**
     * La fonction permet de créé une ligne de commande.
     * @param ligneCommande est un objet ligneCommande.
     */
    @PostMapping("/ligneCommande")
    public void postLigneCommande(@RequestBody LigneCommande ligneCommande){
        ligneCommandeRepository.save(ligneCommande);
    }

    /**
     * La fonction permet d'afficher la liste des lignes de commandes.
     * @return la liste de toutes les lignes de commandes.
     */
    @GetMapping(value = "/ligneCommandes", produces = APPLICATION_JSON_VALUE)
    public Iterable<LigneCommande> getLigneCommande(){
        return ligneCommandeRepository.findAll();
    }

    /**
     * La fonction permet d'afficher une ligne de commande en fonction de son id.
     * @param id est l'id de la ligne de commande à afficher.
     * @return la ligne de commande voulu.
     */
    @GetMapping("/ligneCommande/{id}")
    public LigneCommande getLigneCommandeById(@PathVariable Long id){
        return ligneCommandeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    /**
     * La fonction permet de supprimer une ligne de commande en fonction de son id.
     * @param id est l'id de la ligne de commande à supprimer.
     */
    @DeleteMapping(path = "/ligneCommandes/{id}")
    public void deleteLigneCommande(@PathVariable Long id){
        ligneCommandeRepository.deleteById(id);
    }

    /**
     * La fonction permet de mettre à jour une ligne de commande en fonction de son id.
     * @param id est l'id de la ligne de commande à mettre àjour.
     * @param ligneCommande est un objet ligne de commande.
     * @return
     */
    @PutMapping(path = "/ligneCommande/{id}")
    public LigneCommande updateLigneCommande(@PathVariable long id, @RequestBody LigneCommande ligneCommande){
        LigneCommande updateLigneCommande = ligneCommandeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        updateLigneCommande = ligneCommande;
        ligneCommandeRepository.save(updateLigneCommande);
        return updateLigneCommande;
    }
}
