package com.exam.front.feign;

import com.exam.front.entity.Commande;
import com.exam.front.entity.LigneCommande;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@FeignClient(value = "commande")
public interface CommandeInterface {
    String AUTH_TOKEN = "Authorization";

    @GetMapping("/commandes")
    Iterable<Commande> getCommande(@RequestHeader(AUTH_TOKEN) String token);

    @PostMapping("/commande")
    Commande postCommande(@RequestHeader(AUTH_TOKEN) String token, Commande commande);

    @GetMapping("/commandes/{id}")
    Optional<Commande> getId(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id);

    @PutMapping(path = "/commandes/update/{id}")
    Commande updateCommande(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id, @RequestBody Commande commande);

    @PostMapping("/ligneCommande")
    LigneCommande postligneCommande(@RequestHeader(AUTH_TOKEN) String token, @RequestBody LigneCommande ligneCommande);

    @GetMapping("/ligneCommandes")
    Iterable<LigneCommande> getLigneCommande(@RequestHeader(AUTH_TOKEN) String token);

    @GetMapping("/ligneCommande/{id}")
    Optional<LigneCommande> getLigneCommandeById(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id);
}
