package com.exam.front.feign;

import com.exam.front.entity.Produits;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@FeignClient(value = "produits")
public interface ProduitsInterface {
    String AUTH_TOKEN = "Authorization";

    @GetMapping("/produits")
    Iterable<Produits> getProduits(@RequestHeader(AUTH_TOKEN) String token);

    @GetMapping(path = "/produits/{id}")
    Optional<Produits> getProduitsId(@RequestHeader(AUTH_TOKEN) String token, @PathVariable Long id);

    @PostMapping("/produit")
    Produits postProduits(@RequestHeader(AUTH_TOKEN) String token, @RequestBody Produits produits);

    @DeleteMapping("/produits/delete/{id}")
    Produits deleteProduits(@PathVariable("id") Long id);

    @PutMapping(path = "/produits/update/{id}")
    Produits updateProduit(@PathVariable("id") Long id, @RequestBody Produits produits);

    @GetMapping("/produits/{id}")
    Optional<Produits> getId(@PathVariable("id") Long id);

}
