package com.exam.front.entity;

public class Produits {

    public long id;

    public String name;

    public String date_valide;

    public String instruction;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate_valide() {
        return date_valide;
    }

    public void setDate_valide(String date_valide) {
        this.date_valide = date_valide;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }
}
