package com.exam.front.home;

import com.exam.front.entity.Commande;
import com.exam.front.entity.LigneCommande;
import com.exam.front.entity.Produits;
import com.exam.front.feign.CommandeInterface;
import com.exam.front.feign.ProduitsInterface;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;


@Controller
class WebController {

    @Autowired
    ProduitsInterface produitsInterface;

    @Autowired
    CommandeInterface commandeInterface;

    /**
     * La fonction permet d'afficher notre page d'accueil.
     * @return page web "home"
     */
    @GetMapping(path = "/")
    public String home() {
        return "home";
    }

    /**
     * La fonction permet de nous afficher l'interface de connexion afin que l'utilisateur à la possibilité de ce connecté ou de créé un compte.
     * @return sur la page d'accueil après la connexion
     */
    @GetMapping(path = "/connexion")
    public String connexion() {
        return "redirect:/";
    }

    /**
     * La fonction permet d'afficher la liste de nos produits.
     * @param request est la requête pour récuperer notre token Keycloak.
     * @param model permet de transmettre des informations à notre page web.
     * @return page web "produits"
     */
    @GetMapping(path = "/produits")
    public String getProduits(HttpServletRequest request, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Iterable<Produits> produits = produitsInterface.getProduits("Bearer "+token);
        model.addAttribute("produits", produits);
        return "produits";
    }

    /**
     * La fonction permet d'afficher le formulaire d'ajout d'un nouveau Produit.
     * @param model permet de transmettre des informations à notre page web.
     * @return page web "AjoutProduit"
     */
    @GetMapping("/AjoutProduit")
    public String getAddProduct(Model model){
        model.addAttribute("produit", new Produits());
        return "AjoutProduit";
    }

    /**
     * La fonction permet de créé un nouveau Produit.
     * @param request est la requête pour récuperer notre token Keycloak.
     * @param model permet de transmettre des informations à notre page web.
     * @param produits est notre objet produit.
     * @return sur la page d'accueil après la création du nouveau produit.
     */
    @PostMapping("/AjoutProduit")
    public String postAddProduct(HttpServletRequest request, Model model, Produits produits){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Produits prod = produitsInterface.postProduits("Bearer "+token, produits);
        model.addAttribute("produit", prod);
        return "redirect:/";
    }

    /**
     * La fonction permet d'afficher les informations du produit que l'on veux modifier à l'aide de son id.
     * @param id est l'id du produit à modifier.
     * @param model permet de transmettre des informations à notre page web.
     * @return page web "updateProduit"
     */
    @GetMapping("/produits/update/{id}")
    public String updateProduit(@PathVariable("id") Long id, Model model){
        Optional<Produits> produits = produitsInterface.getId(id);
        model.addAttribute("produit", produits.get());
        return "updateProduit";
    }

    /**
     * La fonction permet de mettre à jour notre produit à l'aide de son id.
     * @param id est l'id du produit à modifier.
     * @param model permet de transmettre des informations à notre page web.
     * @param produits est notre objet produit.
     * @return sur la page d'accueil après la mise à jour du produit.
     */
    @PostMapping("/produits/update/{id}")
    public String updateProduit(@PathVariable("id") Long id, Model model, Produits produits){
        Produits updateproduit = produitsInterface.updateProduit(id, produits);
        model.addAttribute("produit", updateproduit);
        return "redirect:/";
    }


    /**
     * La fonction permet de supprimer un produit à l'aide de son id.
     * @param id est l'id du produit à supprimer.
     * @return sur la page d'accueil après la suppression du produit.
     */
    @GetMapping("/produits/delete/{id}")
    public String deleteProduits(@PathVariable("id") Long id) {
        Produits produits = produitsInterface.deleteProduits(id);
        return "redirect:/";
    }

    /**
     * La fonction permet d'afficher la listes de nos commandes.
     * @param request est la requête pour récuperer notre token Keycloak.
     * @param model permet de transmettre des informations à notre page web.
     * @return page web "commandes".
     */
    @GetMapping(path = "/commandes")
    public String getCommande(HttpServletRequest request, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Iterable<Commande> commande = commandeInterface.getCommande("Bearer "+token);
        model.addAttribute("commandes", commande);
        return "commandes";
    }

    /**
     * La fonction permet d'afficher le formulaire d'ajout de ligne de commande à une commande en fonction de l'id de la ligne de commande.
     * @param request est la requête pour récuperer notre token Keycloak.
     * @param model permet de transmettre des informations à notre page web.
     * @param id est l'id de la ligne de commande a ajouter à la commande.
     * @return page web "AjoutCommande"
     */
    @GetMapping("/AjoutCommande/{id}")
    public String getAjoutCommande(HttpServletRequest request, Model model, @PathVariable("id") Long id){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        model.addAttribute("commande", new Commande());
        Optional<LigneCommande> ligneCommande = commandeInterface.getLigneCommandeById("Bearer "+token, id);
        model.addAttribute("ligneCommande", ligneCommande.get());
        return "AjoutCommande";
    }

    /**
     * La fonction permet d'ajouter une ligne de commande à une commande.
     * @param request est la requête pour récuperer notre token Keycloak.
     * @param model permet de transmettre des informations à notre page web.
     * @param commande est un objet commande.
     * @return sur la page des commandes après avoir créé la commande.
     */
    @PostMapping("AjoutCommande")
    public String postCommande(HttpServletRequest request, Model model, Commande commande){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Commande commande1 = commandeInterface.postCommande("Bearer "+token, commande);
        model.addAttribute("commande", commande1);
        return "redirect:/commandes";
    }

    /**
     * La fonction permet d'afficher le formulaire pour créé une commande à la main.
     * @param request est la requête pour récuperer notre token Keycloak.
     * @param model permet de transmettre des informations à notre page web.
     * @return page web "AjoutSimpleCommande"
     */
    @GetMapping("/AjoutSimpleCommande")
    public String getAddCommande(HttpServletRequest request, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        model.addAttribute("commande", new Commande());
        return "AjoutSimpleCommande";
    }

    /**
     * La fonction permet d'ajouter une commande.
     * @param request est la requête pour récuperer notre token Keycloak.
     * @param model permet de transmettre des informations à notre page web.
     * @param commande est un objet commande.
     * @return sur la page des commandes après la création de la commande.
     */
    @PostMapping("/AjoutSimpleCommande")
    public String postAddCommande(HttpServletRequest request, Model model, Commande commande){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Commande com = commandeInterface.postCommande("Bearer "+token, commande);
        model.addAttribute("commande", com);
        return "redirect:/commandes";
    }

    /**
     * La fonction permet d'afficher les informations de la commande que l'on veux modifier à l'aide de son id.
     * @param request est la requête pour récuperer notre token Keycloak.
     * @param model permet de transmettre des informations à notre page web.
     * @param id est l'id de la commande à modifier.
     * @return page web "updateCommande"
     */
    @GetMapping("/commandes/update/{id}")
    public String updateCommande(HttpServletRequest request, Model model, @PathVariable("id") Long id){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Optional<Commande> commande = commandeInterface.getId("Bearer "+token,id);
        model.addAttribute("commandes", commande.get());
        return "updateCommande";
    }

    /**
     * La fonction permet de mettre à jour notre commande à l'aide de son id.
     * @param request est la requête pour récuperer notre token Keycloak.
     * @param model permet de transmettre des informations à notre page web.
     * @param id est l'id de la commande à modifier.
     * @param commande est notre objet commande.
     * @return sur la page d'accueil après la mise à jour de notre commande.
     */
    @PostMapping("/commandes/update/{id}")
    public String updateCommande(HttpServletRequest request, Model model, @PathVariable("id") Long id, Commande commande){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Commande updatecommande = commandeInterface.updateCommande("Bearer "+token, id, commande);
        model.addAttribute("commande", updatecommande);
        return "redirect:/";
    }

    /**
     * La fonction permet d'afficher le formulaire d'ajout de produit à une ligne de commande en fonction de l'id du produit.
     * @param request est la requête pour récuperer notre token Keycloak.
     * @param model permet de transmettre des informations à notre page web.
     * @param id est l'id du produit.
     * @return page web "AjoutLigneCommande"
     */
    @GetMapping(path = "/AjoutLigneCommande/{id}")
    public String getAjoutLigneCommande(HttpServletRequest request, Model model, @PathVariable("id") Long id){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        model.addAttribute("ligneCommande", new LigneCommande());
        Optional<Produits> produits = produitsInterface.getProduitsId("Bearer "+token, id);
        model.addAttribute("produit", produits.get());
        return "AjoutLigneCommande";
    }

    /**
     * La fonction permet créé une ligne de commande avec l'id du produit et la quantité souhaitée.
     * @param request est la requête pour récuperer notre token Keycloak.
     * @param model permet de transmettre des informations à notre page web.
     * @param ligneCommande est notre objet ligne de commande.
     * @return sur la page des produits après la création de notre ligne de commande.
     */
    @PostMapping(path = "/AjoutLigneCommande")
    public String postLigneCommande(HttpServletRequest request, Model model, LigneCommande ligneCommande){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        LigneCommande ligneCommande1 = commandeInterface.postligneCommande("Bearer "+token, ligneCommande);
        model.addAttribute("ligneCommande", ligneCommande1);
        return "redirect:/produits";
    }

    /**
     * La fonction permet d'afficher la liste de nos lignes de commandes.
     * @param request est la requête pour récuperer notre token Keycloak.
     * @param model permet de transmettre des informations à notre page web.
     * @return page web "panier"
     */
    @GetMapping(path = "/ligneCommandes")
    public String getLigneCommande(HttpServletRequest request, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Iterable<LigneCommande> ligneCommandes = commandeInterface.getLigneCommande("Bearer "+token);
        model.addAttribute("ligneCommande", ligneCommandes);
        return "panier";
    }
}
