package com.exam.produits.produits;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProduitsRepository extends CrudRepository<Produits, Long> {
}
