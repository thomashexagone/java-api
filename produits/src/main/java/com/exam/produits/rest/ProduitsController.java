package com.exam.produits.rest;

import com.exam.produits.produits.Produits;
import com.exam.produits.produits.ProduitsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class ProduitsController {

    @Autowired
    ProduitsRepository produitsRepository;

    /**
     * La fonction permet de créé un objet produits.
     * @param produits est notre objet produits.
     */
    @PostMapping(path = "/produit", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public void postProduit(@RequestBody Produits produits){
        produitsRepository.save(produits);
    }

    /**
     * La fonction permet d'afficher la liste des produits.
     * @return la liste des produits
     */
    @GetMapping(value = "/produits", produces = APPLICATION_JSON_VALUE)
    public Iterable<Produits> getProduits(){
        return produitsRepository.findAll();
    }

    /**
     * La fonction permet d'afficher un produit en fonction de son id.
     * @param id est l'id du produit voulu.
     * @return le produit voulu en fonction de son id.
     */
    @GetMapping(path = "/produits/{id}")
    public Optional<Produits> getProduitId(@PathVariable Long id){
        return produitsRepository.findById(id);
    }

    /**
     * La fonction permet de supprimer un produit en fonction de son id.
     * @param id est l'id du produit à supprimer.
     */
    @DeleteMapping(path = "/produits/delete/{id}")
    public void deleteProduit(@PathVariable Long id){
        produitsRepository.deleteById(id);
    }

    /**
     * La fonction permet de mettre à jour un produit en fonction de son id.
     * @param id est l'id du produit voulu.
     * @param produits est un objet produits.
     * @return le produit avec les nouvelles valeurs.
     */
    @PutMapping(path = "/produits/update/{id}")
    public Produits updateProduits(@PathVariable Long id, @RequestBody Produits produits){
        Produits updateproduits = produitsRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        updateproduits.name = produits.name;
        updateproduits.instruction = produits.instruction;
        produitsRepository.save(updateproduits);
        return updateproduits;
    }
}